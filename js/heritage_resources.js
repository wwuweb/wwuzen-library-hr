

jQuery(document).ready(function() {
	jQuery("ul.slides li").each(function(index) {
		// go through each slide, and look for a link in the title, and wrap the image with that link, and use the link text as the image title.
		// console.log(index);
		var $target_href = jQuery(this).find("div.flex-caption a");
		if ($target_href) {
			var $target_url = $target_href.attr("href");
			var $target_text = $target_href.text();
			// console.log($target_url);
			// console.log($target_text);
			var img_link = "<a href='" + $target_url + "'>";
			jQuery(this).find("img").wrap(img_link);
			jQuery(this).find("img").attr("title", $target_text);
		}
	})
});
